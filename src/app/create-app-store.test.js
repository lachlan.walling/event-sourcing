import * as fs from 'fs'
import * as path from 'path'
import { addTodo } from '../todos/todos-actions'
import { addUser } from '../users/users-actions'
import createAppStore from './create-app-store'

const createCounterIdGenerator =
  (prefix, count = 0) =>
  () =>
    `${prefix}-${count++}`

describe(`app.js`, () => {
  it.skip(`Defaults the initial state correctly`, () => {
    const store = createAppStore()
    expect(store.getState()).toEqual({
      users: { userIndex: {} },
      todos: { todosUserIndex: {} },
    })
  })

  it.skip(`Allows overriding the initial state`, () => {
    const initialState = {
      users: { userIndex: { 'user-0': [] } },
    }
    const store = createAppStore({ initialState })
    expect(store.getState()).toBe(initialState)
  })

  it.skip(`Runs events over all parts of the application`, () => {
    const userIdGenerator = createCounterIdGenerator('user')
    const todoIdGenerator = createCounterIdGenerator('todo')
    const store = createAppStore()

    store.dispatch({
      type: 'users/USER_ADDED',
      payload: {
        id: userIdGenerator(),
        name: 'user',
        email: 'user@email.com',
      },
    })

    expect(store.getState()).toEqual({
      users: {
        userIndex: {
          'user-0': { id: 'user-0', name: 'user', email: 'user@email.com' },
        },
      },
      todos: { todosUserIndex: {} },
    })

    store.dispatch({
      type: 'todos/TODO_ADDED',
      payload: {
        user: 'user-0',
        id: todoIdGenerator(),
        name: 'My todo',
        completed: false,
      },
    })

    expect(store.getState()).toEqual({
      users: {
        userIndex: {
          'user-0': { id: 'user-0', name: 'user', email: 'user@email.com' },
        },
      },
      todos: {
        todosUserIndex: {
          'user-0': [{ id: 'todo-0', name: 'My todo', completed: false }],
        },
      },
    })
  })

  it.skip(`Runs actions over all parts of the application`, () => {
    const userIdGenerator = createCounterIdGenerator('user')
    const todoIdGenerator = createCounterIdGenerator('todo')
    const store = createAppStore()

    store.dispatch(
      addUser({
        id: userIdGenerator(),
        name: 'user',
        email: 'user@email.com',
      }),
    )

    expect(store.getState()).toEqual({
      users: {
        userIndex: {
          'user-0': { id: 'user-0', name: 'user', email: 'user@email.com' },
        },
      },
      todos: { todosUserIndex: {} },
    })

    store.dispatch(
      addTodo({
        user: 'user-0',
        id: todoIdGenerator(),
        name: 'My todo',
        completed: false,
      }),
    )

    expect(store.getState()).toEqual({
      users: {
        userIndex: {
          'user-0': { id: 'user-0', name: 'user', email: 'user@email.com' },
        },
      },
      todos: {
        todosUserIndex: {
          'user-0': [{ id: 'todo-0', name: 'My todo', completed: false }],
        },
      },
    })
  })

  it.skip(`can save an event log`, () => {
    const userIdGenerator = createCounterIdGenerator('user')
    const todoIdGenerator = createCounterIdGenerator('todo')
    const store = createAppStore()

    store.dispatch(
      addUser({
        id: userIdGenerator(),
        name: 'user',
        email: 'user@email.com',
      }),
    )

    store.dispatch(
      addTodo({
        user: 'user-0',
        id: todoIdGenerator(),
        name: 'My todo',
        completed: false,
      }),
    )

    expect(store.getEventLog()).toMatchInlineSnapshot(`
      [
        {
          "payload": {
            "email": "user@email.com",
            "id": "user-0",
            "name": "user",
          },
          "type": "users/USER_ADDED",
        },
        {
          "payload": {
            "completed": false,
            "id": "todo-0",
            "name": "My todo",
            "user": "user-0",
          },
          "type": "todos/TODO_ADDED",
        },
      ]
    `)

    fs.writeFileSync(
      path.join(__dirname, 'tests', 'event-log-write.json'),
      JSON.stringify(store.getEventLog(), null, 2),
    )
  })

  it.skip(`can load an event log`, () => {
    const eventLog = JSON.parse(
      fs.readFileSync(
        path.join(__dirname, 'tests', 'event-log-read.json'),
        'utf-8',
      ),
    )

    const store = createAppStore()
    store.dispatch({
      type: 'REPLAY',
      payload: {
        events: eventLog,
      },
    })

    expect(store.getState()).toMatchInlineSnapshot(`
      {
        "todos": {
          "todosUserIndex": {
            "user-0": [
              {
                "completed": false,
                "id": "todo-0",
                "name": "My todo",
              },
            ],
          },
        },
        "users": {
          "userIndex": {
            "user-0": {
              "email": "user@email.com",
              "id": "user-0",
              "name": "user",
            },
          },
        },
      }
    `)
  })
})
