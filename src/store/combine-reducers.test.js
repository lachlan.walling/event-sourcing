import combineReducers from './combine-reducers'

describe(`combine-reducers.test.js`, () => {
  describe(`Given an empty reducer map`, () => {
    it.skip(`Creates an identity reducer`, () => {
      const reducer = combineReducers({})
      expect(reducer({}, {})).toEqual({})
    })
  })

  describe(`Given a reducer in the reducer map`, () => {
    it.skip(`invokes the reducer under a slice of the state corresponding to its key`, () => {
      const reducer = combineReducers({
        counterA: (state = 0, { type, payload: { amount } }) => {
          if (type === 'addA') return state + amount
          else return state
        },
      })

      expect(
        reducer(
          {},
          {
            type: 'addA',
            payload: { amount: 1 },
          },
        ),
      ).toEqual({
        counterA: 1,
      })
    })
  })

  describe(`Given multiple reducers in the reducer map`, () => {
    it.skip(`invokes each reducer under its own slice of the state corresponding to its key`, () => {
      const reducer = combineReducers({
        counterA: (state = 0, { type, payload: { amount } }) => {
          if (type === 'addA') return state + amount
          else return state
        },
        counterB: (state = 0, { type, payload: { amount } }) => {
          if (type === 'addB') return state + amount
          else return state
        },
      })

      const events = [
        {
          type: 'addA',
          payload: { amount: 1 },
        },
        {
          type: 'addB',
          payload: { amount: 2 },
        },
      ]

      expect(events.reduce(reducer, {})).toEqual({
        counterA: 1,
        counterB: 2,
      })
    })
  })

  describe(`Given an event that doesn't modify the state`, () => {
    it.skip(`returns a state with the same identity as the old state`, () => {
      const reducer = combineReducers({
        counterA: (state = 0, { type, payload: { amount } }) => {
          if (type === 'addA') return state + amount
          else return state
        },
        counterB: (state = 0, { type, payload: { amount } }) => {
          if (type === 'addB') return state + amount
          else return state
        },
      })

      const events = [
        {
          type: 'addA',
          payload: { amount: 1 },
        },
        {
          type: 'addB',
          payload: { amount: 2 },
        },
      ]

      const state = events.reduce(reducer, {})
      const newState = reducer(state, {
        type: 'unknown',
        payload: {},
      })
      expect(state).toBe(newState)
    })
  })
})
