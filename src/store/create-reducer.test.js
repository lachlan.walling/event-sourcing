import createReducer from './create-reducer'
describe(`create-reducer.js`, () => {
  describe(`Given an empty event map`, () => {
    it.skip(`Creates an identity reducer`, () => {
      const reducer = createReducer({})
      expect(reducer({}, {})).toEqual({})
    })
  })

  describe(`Given a default state`, () => {
    it.skip(`defaults the state to the provided value when the state is undefined`, () => {
      const reducer = createReducer({}, [])
      expect(reducer(undefined, {})).toEqual([])
    })
  })

  describe(`Given a event map`, () => {
    it.skip(`Dispatches to the correct reducer`, () => {
      const reducer1 = jest.fn(() => ({ executed: 'event1' }))
      const reducer2 = jest.fn(() => ({ executed: 'event2' }))
      const reducer = createReducer({ event1: reducer1, event2: reducer2 })

      {
        const state = {}
        const event = {
          type: 'event1',
        }
        expect(reducer(state, event)).toEqual({ executed: 'event1' })
        expect(reducer1).toHaveBeenCalledWith(state, event)
      }

      {
        const state = {}
        const event = {
          type: 'event2',
        }
        expect(reducer(state, event)).toEqual({ executed: 'event2' })
        expect(reducer2).toHaveBeenCalledWith(state, event)
      }
    })
  })
})
