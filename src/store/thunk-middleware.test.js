import createStore from './create-store'

import thunkMiddleware from './thunk-middleware'

// timeout
// vs action creators

describe(`store-thunk.js`, () => {
  describe(`Given an event`, () => {
    it.skip(`dispatches normally`, () => {
      const reducer = (state, { payload: { value } }) => state + value

      const store = createStore({
        initialState: 0,
        reducer,
        middleware: [thunkMiddleware],
      })

      store.dispatch({
        type: 'ADD',
        payload: {
          value: 2,
        },
      })

      expect(store.getState()).toEqual(2)
    })
  })

  describe(`Given an action`, () => {
    it.skip(`the action is resolved into an event`, () => {
      const reducer = (state, { payload: { value } }) => state + value

      const action = (value) => (dispatch) =>
        dispatch({ type: 'ADD', payload: { value } })

      const store = createStore({
        initialState: 0,
        reducer,
        middleware: [thunkMiddleware],
      })

      store.dispatch(action(2))

      expect(store.getState()).toEqual(2)
    })

    it.skip(`the action can dispatch multiple events or actions`, () => {
      const reducer = (state, { payload: { value } }) => state + value

      const addAction = (value) => (dispatch) =>
        dispatch({ type: 'ADD', payload: { value } })

      const testAction = () => (dispatch) => {
        dispatch({ type: 'ADD', payload: { value: 1 } })
        dispatch(addAction(2))
        dispatch({ type: 'ADD', payload: { value: 3 } })
        dispatch(addAction(4))
      }

      const store = createStore({
        initialState: 0,
        reducer,
        middleware: [thunkMiddleware],
      })

      store.dispatch(testAction())

      expect(store.getState()).toEqual(10)
    })

    it.skip(`the action is passed getState`, () => {
      const reducer = (state, { payload: { value } }) => state + value

      const testAction = (value) => (dispatch, getState) => {
        if (getState() < 2) dispatch({ type: 'ADD', payload: { value } })
      }

      const store = createStore({
        initialState: 3,
        reducer,
        middleware: [thunkMiddleware],
      })

      store.dispatch(testAction())

      expect(store.getState()).toEqual(3)
    })
  })
})
