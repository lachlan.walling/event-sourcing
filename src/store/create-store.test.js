import createStore from './create-store'

describe(`store.js`, () => {
  it.skip(`creates a store with an initial state`, () => {
    const store = createStore({
      initialState: 0,
    })

    expect(store.getState()).toEqual(0)
  })

  it.skip(`dispatches an event to a reducer`, () => {
    const reducer = (state, { payload: { value } }) => state + value
    const store = createStore({
      initialState: 0,
      reducer,
    })

    store.dispatch({
      type: 'ADD',
      payload: {
        value: 2,
      },
    })

    expect(store.getState()).toEqual(2)
  })

  describe(`middleware`, () => {
    it.skip(`Runs the first middleware in the chain`, () => {
      const reducer = (state, { payload: { value } }) => state + value
      let called1,
        called2 = false
      const middleware1 = (store) => (next) => (event) => {
        called1 = true
      }
      const middleware2 = (store) => (next) => (event) => {
        called2 = true
      }

      const store = createStore({
        initialState: 0,
        middleware: [middleware1, middleware2],
        reducer,
      })

      store.dispatch({
        type: 'ADD',
        payload: {
          value: 2,
        },
      })

      expect(called1).toEqual(true)
      expect(called2).toEqual(false)
      expect(store.getState()).toEqual(0)
    })

    it.skip(`next in the first middleware invokes the second`, () => {
      const reducer = (state, { payload: { value } }) => state + value

      let called1 = false
      let called2 = false
      const middleware1 = (store) => (next) => (event) => {
        called1 = true
        next(event)
      }
      const middleware2 = (store) => (next) => (event) => {
        called2 = true
      }

      const store = createStore({
        initialState: 0,
        middleware: [middleware1, middleware2],
        reducer,
      })

      store.dispatch({
        type: 'ADD',
        payload: {
          value: 2,
        },
      })

      expect(called1).toEqual(true)
      expect(called2).toEqual(true)
      expect(store.getState()).toEqual(0)
    })

    it.skip(`next in the second middleware invokes dispatch`, () => {
      const reducer = (state, { payload: { value } }) => state + value

      let called1,
        called2 = false
      const middleware1 = (store) => (next) => (event) => {
        called1 = true
        next(event)
      }
      const middleware2 = (store) => (next) => (event) => {
        called2 = true
        next(event)
      }

      const store = createStore({
        initialState: 0,
        middleware: [middleware1, middleware2],
        reducer,
      })

      store.dispatch({
        type: 'ADD',
        payload: {
          value: 2,
        },
      })

      expect(called1).toEqual(true)
      expect(called2).toEqual(true)
      expect(store.getState()).toEqual(2)
    })
  })
})
