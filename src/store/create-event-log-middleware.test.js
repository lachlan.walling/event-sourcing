import createEventLogMiddleware from './create-event-log-middleware'
import createStore from './create-store'

describe(`event-log-middleware.js`, () => {
  describe(`Given multiple events are dispatched`, () => {
it.skip(`adds it to an event log and dispatches the event`, () => {
      const reducer = (state, { payload: { value } }) => state + value

      const { eventLogMiddleware, getEventLog } = createEventLogMiddleware()

      const store = createStore({
        initialState: 0,
        reducer,
        middleware: [eventLogMiddleware],
      })

      store.dispatch({
        type: 'ADD',
        payload: {
          value: 2,
        },
      })

      store.dispatch({
        type: 'ADD',
        payload: {
          value: 3,
        },
      })

      expect(store.getState()).toEqual(5)

      expect(getEventLog()).toEqual([
        {
          type: 'ADD',
          payload: {
            value: 2,
          },
        },
        {
          type: 'ADD',
          payload: {
            value: 3,
          },
        },
      ])
    })
  })
  describe(`Given a replay event is dispatched`, () => {
it.skip(`dispatches all the events in the payload`, () => {
      const reducer = (state, { type, payload: { value } }) =>
        type === 'ADD' ? state + value : state

      const { eventLogMiddleware, getEventLog } = createEventLogMiddleware()

      const store = createStore({
        initialState: 0,
        reducer,
        middleware: [eventLogMiddleware],
      })

      store.dispatch({
        type: 'REPLAY',
        payload: {
          events: [
            {
              type: 'ADD',
              payload: {
                value: 2,
              },
            },
            {
              type: 'ADD',
              payload: {
                value: 3,
              },
            },
          ],
        },
      })

      expect(store.getState()).toEqual(5)

      expect(getEventLog()).toEqual([
        {
          type: 'ADD',
          payload: {
            value: 2,
          },
        },
        {
          type: 'ADD',
          payload: {
            value: 3,
          },
        },
      ])
    })
  })
})
