import createStore from 'store/create-store'
import usersReducer from './users-reducer'

const createCounterIdGenerator =
  (count = 0) =>
  () =>
    count++

describe(`users-reducer.js`, () => {
  describe(`adding users`, () => {
    it.skip(`adds the user`, () => {
      const store = createStore({
        reducer: usersReducer,
      })
      const idGenerator = createCounterIdGenerator()

      store.dispatch({
        type: 'users/USER_ADDED',
        payload: {
          id: idGenerator(),
          name: 'user1',
          email: 'user1@email.com',
        },
      })

      expect(store.getState()).toEqual({
        userIndex: {
          0: { id: 0, name: 'user1', email: 'user1@email.com' },
        },
      })
    })
    it.skip(`adds multiple users`, () => {
      const store = createStore({
        reducer: usersReducer,
      })
      const idGenerator = createCounterIdGenerator()

      store.dispatch({
        type: 'users/USER_ADDED',
        payload: {
          id: idGenerator(),
          name: 'user',
          email: 'user@email.com',
        },
      })

      store.dispatch({
        type: 'users/USER_ADDED',
        payload: {
          id: idGenerator(),
          name: 'another user',
          email: 'another-user@email.com',
        },
      })

      expect(store.getState()).toEqual({
        userIndex: {
          0: { id: 0, name: 'user', email: 'user@email.com' },
          1: { id: 1, name: 'another user', email: 'another-user@email.com' },
        },
      })
    })
  })

  describe(`given an unknown event`, () => {
    it.skip(`does not modify the state`, () => {
      const initialState = {
        userIndex: {
          0: {
            id: 0,
            email: 'user@email.com',
            name: 'user',
          },
        },
      }
      const store = createStore({
        initialState,
        reducer: usersReducer,
      })
      store.dispatch({
        type: 'UNKNOWN',
        payload: {},
      })
      expect(store.getState()).toBe(initialState)
    })
  })
})
