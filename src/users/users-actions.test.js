import createAppStore from 'app/create-app-store'
import { addUser } from './users-actions'

const createCounterIdGenerator =
  (prefix, count = 0) =>
  () =>
    `${prefix}-${count++}`

describe(`users-reducer.js`, () => {
  describe(`adding users`, () => {
    it.skip(`adds the user`, () => {
      const store = createAppStore()
      const idGenerator = createCounterIdGenerator('user')

      store.dispatch(
        addUser({
          id: idGenerator(),
          name: 'user',
          email: 'user@email.com',
        }),
      )

      expect(store.getState()).toMatchObject({
        users: {
          userIndex: {
            'user-0': { id: 'user-0', name: 'user', email: 'user@email.com' },
          },
        },
      })
    })

    it.skip(`adds multiple users`, () => {
      const store = createAppStore()
      const idGenerator = createCounterIdGenerator('user')

      store.dispatch(
        addUser({
          id: idGenerator(),
          name: 'user',
          email: 'user@email.com',
        }),
      )

      store.dispatch(
        addUser({
          id: idGenerator(),
          name: 'another user',
          email: 'another-user@email.com',
        }),
      )

      expect(store.getState()).toMatchObject({
        users: {
          userIndex: {
            'user-0': { id: 'user-0', name: 'user', email: 'user@email.com' },
            'user-1': {
              id: 'user-1',
              name: 'another user',
              email: 'another-user@email.com',
            },
          },
        },
      })
    })

    it.skip(`validates that the email is unique`, () => {
      const store = createAppStore()
      const idGenerator = createCounterIdGenerator('user')

      store.dispatch(
        addUser({
          id: idGenerator(),
          name: 'user',
          email: 'user@email.com',
        }),
      )

      store.dispatch(
        addUser({
          id: idGenerator(),
          name: 'another user',
          email: 'user@email.com',
        }),
      )

      expect(store.getState()).toMatchObject({
        users: {
          userIndex: {
            'user-0': { id: 'user-0', name: 'user', email: 'user@email.com' },
          },
          error: { message: 'Email is already taken' },
        },
      })
    })
  })

  describe(`given an unknown event`, () => {
    it.skip(`does not modify the state`, () => {
      const initialState = {
        users: {
          userIndex: {
            'user-0': {
              id: 'user-0',
              name: 'user',
              email: 'user@email.com',
            },
          },
        },
      }
      const store = createAppStore({ initialState })
      store.dispatch({
        type: 'UNKNOWN',
        payload: {},
      })
      expect(store.getState().users).toBe(initialState.users)
    })
  })
})
