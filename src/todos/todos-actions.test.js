import createAppStore from '../app/create-app-store'
import { addTodo, deleteTodo } from './todos-actions'

const createCounterIdGenerator =
  (prefix = 'todo', count = 0) =>
  () =>
    `${prefix}-${count++}`

describe(`todos-actions.js`, () => {
  describe(`adding todos`, () => {
    it.skip(`adds the todo`, () => {
      const store = createAppStore()
      const idGenerator = createCounterIdGenerator()

      store.dispatch(
        addTodo({
          id: idGenerator(),
          user: 'user-0',
          name: 'My todo',
          completed: true,
        }),
      )

      expect(store.getState()).toMatchObject({
        todos: {
          todosUserIndex: {
            'user-0': [{ id: 'todo-0', name: 'My todo', completed: true }],
          },
        },
      })
    })

    it.skip(`adds multiple todos`, () => {
      const store = createAppStore()
      const idGenerator = createCounterIdGenerator()

      store.dispatch(
        addTodo({
          user: 'user-0',
          id: idGenerator(),
          name: 'My todo',
          completed: false,
        }),
      )

      store.dispatch(
        addTodo({
          user: 'user-0',
          id: idGenerator(),
          name: 'Another todo',
          completed: true,
        }),
      )

      expect(store.getState()).toMatchObject({
        todos: {
          todosUserIndex: {
            'user-0': [
              { id: 'todo-0', name: 'My todo', completed: false },
              { id: 'todo-1', name: 'Another todo', completed: true },
            ],
          },
        },
      })
    })
  })

  describe(`removing todos`, () => {
    it.skip(`removes a todo`, () => {
      const initialState = {
        todos: {
          todosUserIndex: {
            'user-0': [
              { id: 'todo-0', name: 'My todo', completed: false },
              { id: 'todo-1', name: 'Another todo', completed: true },
            ],
          },
        },
      }

      const store = createAppStore({ initialState })
      store.dispatch(
        deleteTodo({
          user: 'user-0',
          id: 'todo-0',
        }),
      )

      expect(store.getState()).toMatchObject({
        todos: {
          todosUserIndex: {
            'user-0': [{ id: 'todo-1', name: 'Another todo', completed: true }],
          },
        },
      })
    })

    describe(`given an id that does not exist`, () => {
      it.skip(`returns the same state`, () => {
        const initialState = {
          todos: {
            todosUserIndex: {
              'user-0': [
                { id: 'todo-0', name: 'My todo', completed: false },
                { id: 'todo-1', name: 'Another todo', completed: true },
              ],
            },
          },
        }

        const store = createAppStore({ initialState })
        store.dispatch(
          deleteTodo({
            user: 'user-0',
            id: 'todo-2',
          }),
        )

        expect(store.getState().todos).toBe(initialState.todos)
      })
    })
  })

  describe(`given an unknown event`, () => {
    it.skip(`does not modify the state`, () => {
      const initialState = {
        todos: {
          todosUserIndex: {
            'user-0': [
              { id: 'todo-0', name: 'My todo', completed: false },
              { id: 'todo-1', name: 'Another todo', completed: true },
            ],
          },
        },
      }

      const store = createAppStore({ initialState })

      store.dispatch({
        type: 'UNKNOWN',
        payload: {},
      })

      expect(store.getState().todos).toBe(initialState.todos)
    })
  })
})
