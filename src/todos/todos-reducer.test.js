import createStore from 'store/create-store'
import todosReducer from './todos-reducer'

const createCounterIdGenerator =
  (prefix = 'todo', count = 0) =>
  () =>
    `${prefix}-${count++}`

describe(`todos-reducer.js`, () => {
  describe(`adding todos`, () => {
    it.skip(`adds the todo`, () => {
      const store = createStore({
        reducer: todosReducer,
      })
      const idGenerator = createCounterIdGenerator()

      store.dispatch({
        type: 'todos/TODO_ADDED',
        payload: {
          id: idGenerator(),
          user: 'user-0',
          name: 'My todo',
          completed: true,
        },
      })

      expect(store.getState()).toEqual({
        todosUserIndex: {
          'user-0': [{ id: 'todo-0', name: 'My todo', completed: true }],
        },
      })
    })

    it.skip(`adds multiple todos`, () => {
      const store = createStore({
        reducer: todosReducer,
      })
      const idGenerator = createCounterIdGenerator()

      store.dispatch({
        type: 'todos/TODO_ADDED',
        payload: {
          id: idGenerator(),
          name: 'My todo',
          user: 'user-0',
          completed: false,
        },
      })

      store.dispatch({
        type: 'todos/TODO_ADDED',
        payload: {
          id: idGenerator(),
          name: 'Another todo',
          user: 'user-0',
          completed: true,
        },
      })

      expect(store.getState()).toEqual({
        todosUserIndex: {
          'user-0': [
            { id: 'todo-0', name: 'My todo', completed: false },
            { id: 'todo-1', name: 'Another todo', completed: true },
          ],
        },
      })
    })
  })

  describe(`removing todos`, () => {
    it.skip(`removes a todo`, () => {
      const initialState = {
        todosUserIndex: {
          'user-0': [
            {
              id: 'todo-0',
              name: 'My todo',
              completed: true,
            },
          ],
        },
      }

      const store = createStore({
        initialState,
        reducer: todosReducer,
      })
      store.dispatch({
        type: 'todos/TODO_REMOVED',
        payload: {
          user: 'user-0',
          id: 'todo-0',
        },
      })

      expect(store.getState()).toEqual({
        todosUserIndex: {
          'user-0': [],
        },
      })
    })
    describe(`given an id that does not exist`, () => {
      it.skip(`returns the same state`, () => {
        const initialState = {
          todosUserIndex: {
            'user-0': [
              {
                id: 'todo-0',
                name: 'My todo',
                completed: true,
              },
            ],
          },
        }

        const store = createStore({
          initialState,
          reducer: todosReducer,
        })
        store.dispatch({
          type: 'todos/TODO_REMOVED',
          payload: {
            user: 'user-0',
            id: 'todo-1',
          },
        })

        expect(store.getState()).toBe(initialState)
      })
    })
  })

  describe(`given an unknown event`, () => {
    it.skip(`does not modify the state`, () => {
      const initialState = []
      const store = createStore({
        initialState,
        reducer: todosReducer,
      })
      store.dispatch({
        type: 'UNKNOWN',
        payload: {},
      })
      expect(store.getState()).toBe(initialState)
    })
  })
})
