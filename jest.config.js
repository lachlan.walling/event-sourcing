module.exports = {
  moduleDirectories: ['./src', 'node_modules'],
  testPathIgnorePatterns: ['src/app/tests/event-log-write.json'],
  transform: {
    '^.+\\.jsx?$': [
      '@swc/jest',
      {
        sourceMaps: true,
        jsc: {
          parser: {
            syntax: 'ecmascript',
            jsx: true,
            dynamicImport: true,
            privateMethod: true,
            functionBind: true,
            classPrivateProperty: true,
            exportDefaultFrom: true,
            exportNamespaceFrom: true,
            decorators: true,
            decoratorsBeforeExport: true,
            importMeta: true,
          },
        },
      },
    ],
  },
}
